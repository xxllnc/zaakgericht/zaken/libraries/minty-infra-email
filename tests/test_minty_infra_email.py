# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_infra_email import infrastructure
from unittest import mock


def test_email_infrastructure():
    config = {
        "email": {"smarthost_hostname": "localhost", "smarthost_port": 123}
    }
    infra = infrastructure.EmailInfrastructure(config)

    assert infra.default_smtp_smarthost == "localhost"
    assert infra.default_smtp_smarthost_port == 123
    assert infra.default_smtp_starttls is False

    config = {
        "email": {
            "smarthost_hostname": "localhost",
            "smarthost_port": 123,
            "smarthost_starttls": 1,
        }
    }
    infra = infrastructure.EmailInfrastructure(config)

    assert infra.default_smtp_smarthost == "localhost"
    assert infra.default_smtp_smarthost_port == 123
    assert infra.default_smtp_starttls is True

    config = {"email": {"smarthost_hostname": "localhost"}}
    infra = infrastructure.EmailInfrastructure(config)

    assert infra.default_smtp_smarthost == "localhost"
    assert infra.default_smtp_smarthost_port == 25
    assert infra.default_smtp_starttls is False


def test_outgoing_email_merge_configuration():
    infra = infrastructure.OutgoingEmail(
        default_smtp_smarthost="localhost", default_smtp_smarthost_port=2525
    )

    email_config = infrastructure.EmailConfiguration(
        smarthost_hostname="not-localhost",
        smarthost_port=576,
        smarthost_username="somebody",
        smarthost_password="secret",
        smarthost_security="starttls",
    )

    merged = infra._merge_configuration(email_config)
    assert merged.smarthost_hostname == "not-localhost"
    assert merged.smarthost_port == 576
    assert merged.smarthost_username == "somebody"
    assert merged.smarthost_password == "secret"
    assert merged.smarthost_security == "starttls"


def test_outgoing_email_merge_configuration_no_smarthost():
    infra = infrastructure.OutgoingEmail(
        default_smtp_smarthost="localhost", default_smtp_smarthost_port=2525
    )

    email_config = infrastructure.EmailConfiguration(
        smarthost_hostname=None,
        smarthost_port=576,
        smarthost_username="somebody",
        smarthost_password="secret",
        smarthost_security="starttls",
    )

    merged = infra._merge_configuration(email_config)
    assert merged.smarthost_hostname == "localhost"
    assert merged.smarthost_port == 2525
    assert merged.smarthost_username is None
    assert merged.smarthost_password is None
    assert merged.smarthost_security == "none"


@mock.patch("minty_infra_email.infrastructure.SMTP")
def test_outgoing_email_send(mock_smtp):
    infra = infrastructure.OutgoingEmail(
        default_smtp_smarthost="localhost", default_smtp_smarthost_port=2525
    )

    email_config = infrastructure.EmailConfiguration(
        smarthost_hostname="smartest-host",
        smarthost_port=576,
        smarthost_username="somebody",
        smarthost_password="secret",
        smarthost_security="starttls",
    )

    message = mock.MagicMock()
    rv = infra.send(message, email_config)

    assert rv is None

    mock_smtp.assert_called_once_with(host="smartest-host", port=576)
    mock_smtp().__enter__.assert_called_once_with()

    mock_smtp().__enter__().starttls.assert_called_once_with()
    mock_smtp().__enter__().login.assert_called_once_with(
        user="somebody", password="secret"
    )
    mock_smtp().__enter__().send_message.assert_called_once_with(message)
    mock_smtp().__exit__.assert_called_once_with(None, None, None)


@mock.patch("minty_infra_email.infrastructure.SMTP")
def test_outgoing_email_send_empty_username(mock_smtp):
    infra = infrastructure.OutgoingEmail(
        default_smtp_smarthost="localhost", default_smtp_smarthost_port=2525
    )

    email_config = infrastructure.EmailConfiguration(
        smarthost_hostname="smartest-host",
        smarthost_port=576,
        smarthost_username="",
        smarthost_password="",
        smarthost_security="starttls",
    )

    message = mock.MagicMock()
    rv = infra.send(message, email_config)

    assert rv is None

    mock_smtp.assert_called_once_with(host="smartest-host", port=576)
    mock_smtp().__enter__.assert_called_once_with()

    mock_smtp().__enter__().starttls.assert_called_once_with()
    mock_smtp().__enter__().login.assert_not_called()
    mock_smtp().__enter__().send_message.assert_called_once_with(message)
    mock_smtp().__exit__.assert_called_once_with(None, None, None)


@mock.patch("minty_infra_email.infrastructure.SMTP")
def test_outgoing_email_send_default_smtp(mock_smtp):
    infra = infrastructure.OutgoingEmail(
        default_smtp_smarthost="localhost", default_smtp_smarthost_port=2525
    )

    email_config = infrastructure.EmailConfiguration(
        smarthost_hostname=None,
        smarthost_port=25,
        smarthost_username=None,
        smarthost_password=None,
        smarthost_security="none",
    )

    message = mock.MagicMock()
    rv = infra.send(message, email_config)

    assert rv is None

    mock_smtp.assert_called_once_with(host="localhost", port=2525)
    mock_smtp().__enter__.assert_called_once_with()
    mock_smtp().__enter__().send_message.assert_called_once_with(message)
    mock_smtp().__exit__.assert_called_once_with(None, None, None)


@mock.patch("minty_infra_email.infrastructure.SMTP")
def test_outgoing_email_send_oauth2(mock_smtp_cls):
    mock_smtp = mock.Mock()
    mock_smtp_cls().__enter__.return_value = mock_smtp
    mock_smtp_cls.reset_mock()

    infra = infrastructure.OutgoingEmail(
        default_smtp_smarthost="localhost", default_smtp_smarthost_port=2525
    )

    email_config = infrastructure.EmailConfiguration(
        smarthost_mode="oauth2",
        smarthost_hostname="example.com",
        smarthost_port=25,
        smarthost_username="some_username",
        smarthost_token="abcdefg",
        smarthost_security="none",
    )

    message = mock.Mock()
    rv = infra.send(message, email_config)

    assert rv is None

    mock_smtp_cls.assert_called_once_with(host="example.com", port=25)
    mock_smtp.auth.assert_called_once_with(
        mechanism="XOAUTH2",
        authobject=mock.ANY,
        initial_response_ok=False,
    )
    mock_smtp.send_message.assert_called_once_with(message)

    # Check that the XOAUTH2 response is built correctly
    authobject = mock_smtp.auth.call_args[1]["authobject"]
    xoauth = authobject()
    assert xoauth == "user=some_username\x01auth=Bearer abcdefg\x01\x01"
